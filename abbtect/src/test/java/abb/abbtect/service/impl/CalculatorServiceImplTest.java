package abb.abbtect.service.impl;

import abb.abbtect.dto.CalculatorResponse;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class CalculatorServiceImplTest {

    @Mock
    private CalculatorResponse mockedResponse;

    @InjectMocks
    private CalculatorServiceImpl calculatorService;

    @Test
    public void testAdd() {
        int a = 5;
        int b = 3;
        int expectedSum = a + b;
        when(calculatorService.add(a,b)).thenReturn(CalculatorResponse.builder().result(expectedSum).build());
        CalculatorResponse result = calculatorService.add(a, b);
        verify(mockedResponse).setResult(expectedSum);
        assertEquals(expectedSum, result.getResult());
    }

}