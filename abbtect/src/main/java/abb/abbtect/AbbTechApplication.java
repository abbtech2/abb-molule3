package abb.abbtect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbbTechApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbTechApplication.class, args);
    }

}
