package abb.abbtect.controller;

import abb.abbtect.dto.CalculatorRequest;
import abb.abbtect.dto.CalculatorResponse;
import abb.abbtect.service.CalculatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api")
@RequiredArgsConstructor
public class CalculatorController {


    private final CalculatorService calculatorService;

    @PostMapping(path = "/add")
    public CalculatorResponse topla(@RequestBody CalculatorRequest calculator){
        CalculatorResponse add = calculatorService.add(calculator.getA(), calculator.getB());
        return add;
    }

    @PostMapping(path = "/multiplication")
    public CalculatorResponse hasil(@RequestBody CalculatorRequest calculator){
        CalculatorResponse multi = calculatorService.multiplication(calculator.getA(), calculator.getB());
        return multi;
    }

    @PostMapping(path = "/division")
    public CalculatorResponse bol(@RequestBody CalculatorRequest calculator){
        CalculatorResponse div = calculatorService.division(calculator.getA(), calculator.getB());
        return div;
    }

    @PostMapping(path = "/subtraction")
    public CalculatorResponse subtraction(@RequestBody CalculatorRequest calculator){
        CalculatorResponse sub = calculatorService.subtraction(calculator.getA(), calculator.getB());
        return sub;
    }
}
