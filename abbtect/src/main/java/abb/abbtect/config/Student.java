package abb.abbtect.config;

import lombok.Data;

@Data
public class Student {

    private int id;
    private String name;
}
