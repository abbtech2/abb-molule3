package abb.abbtect.service;

import abb.abbtect.dto.CalculatorResponse;

public interface CalculatorService {

    CalculatorResponse add(int a, int b);

    CalculatorResponse multiplication(int a,int b);
    CalculatorResponse division(int a,int b);

    CalculatorResponse subtraction(int a,int b);
}
