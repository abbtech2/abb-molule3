package abb.abbtect.service.impl;

import abb.abbtect.dto.CalculatorResponse;
import abb.abbtect.service.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    public CalculatorResponse add(int a, int b){
        int cem=a+b;
        return CalculatorResponse.builder()
                .result(cem)
                .build();
    }

    public CalculatorResponse multiplication(int a,int b){
        CalculatorResponse response=new CalculatorResponse();
        int hasil=a*b;
        response.setResult(hasil);
        return response;
    }
    public CalculatorResponse division(int a,int b){
        CalculatorResponse response=new CalculatorResponse();
        int bol=a/b;
        response.setResult(bol);
        return response;
    }

    public CalculatorResponse subtraction(int a,int b){
        CalculatorResponse response=new CalculatorResponse();
        int sub=a-b;
        response.setResult(sub);
        return response;
    }
}
